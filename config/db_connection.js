"use_strict";

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : process.env.DB_HOST || 'localhost',
  user     : process.env.DB_USER || 'root',
  password : process.env.DB_PASS || '',
  database : process.env.DB_BASE || 'my_finances'
});

module.exports = connection;
