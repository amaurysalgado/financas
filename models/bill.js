'use strict';
var Model = require("./model");

class  Bill extends Model{
  constructor(){
    super("bills");
    this.select = this._billSelect();
  }

  getAll(conditions){
    return  this._executeQuery(this._billSelect()).then(x => x.rows);
  }

  insert(billValues){
    super.insert(this._fixParams(billValues));
  }

  update(id, newBillValues){
    super.update(id,this._fixParams(newBillValues));
  }

  _fixParams(newParams){
    newParams["value"] = newParams["value"].split(",").join(".");
    newParams["name"] = "";
    if (newParams["user_id"] == ''){
      newParams["user_id"] = null
    }
    return newParams;
  }

  getInitalObject(){
    let dateYear = Utils.monthYear();
    return {
      name: '',
      value: '',
      month: dateYear["month"],
      year: dateYear["year"],
      user_id: null,
    }
  };

  getLastMonths(billName){
    let query = "SELECT value, concat(month,'/',year) as date from " + this.table_name + " ";
    query += "where bill_type_id = " + this.escapeValue(billName) + " ";
    query += "and user_id is null ";
    query += "order by year desc, month desc ";
    query += "limit 12";
    return this._executeQuery(query).then(function(response){
      let data = { value: [], date: []}
      let rows = response.rows;
      let i = rows.length - 1;
      for(i; i>=0;i--){
        data.value.push(rows[i].value);
        data.date.push(rows[i].date);
      }
      return data;
    });
  }

  getLastMonthBills(callback){
    let self = this;
    return this.lastMonthYear().then(function(date){
      let query = "SELECT `bills_types`.`name` , `bills`.value from bills ";
      query += "INNER JOIN `bills_types` on `bills_types`.id = `bills`.`bill_type_id`";
      query += "where month = " + date.month + " AND year = " + date.year + " ";
      query += "and user_id is null ";
      query += "order by value;"
      return self._executeQuery(query).then(function(resp){
        let data = resp.rows;
        // console.log(data);
        let response = {bills: [], total: 0, month_year:`${date.month}/${date.year}`};
        let i = 0;
        let total = 0;
        for(i; i< data.length; i++){
          response.bills.push([data[i].name, data[i].value]);
          response.total += data[i].value;
        }
        return response;
      });
    });
  }

  lastMonthYear(){
    let query = "SELECT max(month) as last_month, year as last_year from bills ";
    query += "group by year order by year desc limit 1";
    return this._executeQuery(query).then( function(response){
      let row = response.rows;
      return { month: row[0].last_month, year: row[0].last_year};
    });
  }

  _billSelect(){
    let query = "SELECT `bills`.*, `bills_types`.`name` from `bills` ";
    query += "INNER JOIN `bills_types` on `bills_types`.id = `bills`.`bill_type_id`";
    return query;
  }

};
module.exports = new Bill();
