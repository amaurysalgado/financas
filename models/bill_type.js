'use strict';
var Model = require("./model");

class BillType extends Model{
  constructor(){
    super("bills_types");
  }
};
module.exports = new BillType();
