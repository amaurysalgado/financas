"use strict";
var db = require('./../config/db_connection');

class Model {
  constructor(table_name){
    this.table_name = table_name;
    this.fields = [];
    var self = this;
    var query = "SELECT * FROM "+ this.table_name +" limit 1; ";
    this._executeQuery(query)
    .then(function(response){
      var i = 0;
      var length = response.fields.length;
      for (i; i < length; i++){
        self.fields.push(response.fields[i].name)
      }
    });
  }

  _executeQuery(query){
    console.log(query);
    return new Promise(function(fulfill,reject){
      db.query(query, function(err, rows, fields){
        if (err) reject(err);
        else fulfill({rows: rows,fields: fields});
      })
    })
  };

  getTableName(){ return this.table_name; };

  getColumns(){ return this.fields; };

  getById(id){
    let query = `SELECT * FROM ${this.table_name} where ${this.table_name}.id = ${id} ;`;
    if (this.select != null){
      query = this.select;
      query += ' WHERE `'+this.table_name+'`.id = '+db.escape(id)+' limit 1;';
    }
    return this._executeQuery(query).then(x => x.rows[0] || null);
  }

  getAll(conditions){
    let query = this._buildQuery(conditions);
    return  this._executeQuery(query).then(x => x.rows);
  }

  where(options){
    if (options.conditions == null){ throw 'missing the conditions to where clause' }

    let query = this._buildQuery(options);
    return this._executeQuery(query).then(x => x.rows);
  };

  insert(data){
    delete data['id'];
    var fields_to_set = [];
    var key = "";
    for (key in data){
      if (this.fields.includes(key)){
        fields_to_set.push(key+" = "+ db.escape(data[key]));
      }
    }

    var query = "INSERT INTO "+this.table_name+" SET ";
    fields_to_set = fields_to_set.join(", ");
    query = query+fields_to_set;
    this._executeQuery(query);
  };

  update(id, data){
    delete data["id"];
    var fields_to_set = [];
    var key = "";
    for (key in data){
      if (this.fields.includes(key)){
        fields_to_set.push(key+" = "+ db.escape(data[key]));
      }
    }

    var query = "UPDATE "+this.table_name+" SET ";
    fields_to_set = fields_to_set.join(", ");
    query = query+fields_to_set;
    query = query+" WHERE id = "+db.escape(id);
    return this._executeQuery(query);
  };

  deleteById(id){
    if (id == null){ throw 'missing the id'; }
    let query = "DELETE FROM "+this.table_name;
    query += this._addQueryoptions({conditions: `id = ${db.escape(id)}`});
    return this._executeQuery(query).then(x => x.rows.affectedRows > 0);
  };

  deleteByCondition(condition){
    var query = "DELETE FROM "+this.table_name;
    query += this._addQueryoptions(conditions);
    this._executeQuery(query).then(x => x.rows.affectedRows > 0);
  };

  escapeValue(value){
    return db.escape(value);
  }

  _buildQuery(options = null){
    let queryOptions = Object.assign({select: '*'}, options)
    let query = `SELECT ${queryOptions.select} FROM ${this.table_name}`;
    query += this._addJoins(queryOptions.joins);
    query += this._addQueryoptions(queryOptions);
    query += ';';
    return query
  }

  // @options - should be escaped before call this function
  _addQueryoptions(options){
    let query = this._addConditions(options.conditions);
    query += this._addGroupBy(options.groupBy);
    query += this._addOrderBy(options.orderBy);
    query += this._addLimit(options.limit);
    return query;
  }

  _addJoins(queryJoins){
    return '';
  }

  _addConditions(conditions){
    return (conditions != null) ? ` WHERE (${conditions})` :'';
  }

  _addLimit(limit){
    return (limit != null) ? ` LIMIT ${limit}` : '';
  }

  _addGroupBy(groupBy){
    return (groupBy != null) ? ` GROUP BY ${groupBy}` : '';
  }

  _addOrderBy(orderBy){
    return (orderBy != null) ? ` ORDER BY ${orderBy}` :'';
  }
}

module.exports = Model;
