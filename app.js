// 'use strict'

var port = process.env.PORT || 3001;
var express = require('express')
var app = express()
var expressLayouts = require('express-ejs-layouts');
var bodyParser = require('body-parser')
require("./config/load_models");

String.prototype.capitalize = function() {
  return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};


app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

/* Configuraions */
app.set('view engine', 'ejs');
app.set('layout', 'layouts/layout');
app.use(expressLayouts); /* set layout render */
app.use(express.static(__dirname + '/public')); /* public files */

// logs
app.use(function(req,res,next){
  console.log(req.ip.split(":").pop() + ": " + req.method + " - " + req.originalUrl);
  next();
});

// Routes
var root_path = require('./routes/index');
var metrics_path = require('./routes/metrics');
var users_path = require('./routes/users');
var bills_path = require('./routes/bills');
var bills_types_path = require('./routes/bills_types');

app.use('/', root_path);
app.use('/metrics' ,metrics_path);
app.use('/users', users_path);
app.use('/bills', bills_path);
app.use('/bills_types', bills_types_path);

app.listen(port, function(){
  console.log("Express server listening on port %d", port);
});
