$(document).ready(function(){
  billsTable = $("table#bills");
  if (billsTable.length > 0){
    billsTable = billsTable.DataTable({
      responsive: true,
      ajax: '/bills/get_bills',
      columns: [
        {
          data: "name",
          render: function(data){
            return data.capitalize();
          }
        },
        {data: 'value'},
        {data: 'month'},
        {data: 'year'},
        {
          data: null,
          render: renderBillButtom
        }
      ],
    });
    billsTable.order([[3,'desc'],[2,'desc']])
  }
});

function renderBillButtom(data){
  let buttons = '<div class="btn-group">' +
    '<a href="/bills/'+ data.id +'" class="btn btn-default btn-sm">'+
      '<i class="fa fa-eye" ></i>' +
    '</a>' +
    '<a href="/bills/'+ data.id +'/edit" class="btn btn-primary btn-sm">'+
      '<i class="fa fa-pencil" ></i>'+
    '</a>'+
    "<button class='btn btn-danger btn-sm' onclick='deleteBill(\"/bills/"+data.id+"\")'>" +
      '<i class="fa fa-trash" ></i>'+
    '</a>'+
  '</div>';
  return buttons;
}

function deleteBill(href) {
  $.ajax({
    url: href,
    method: "DELETE",
    success: function(){
      billsTable.ajax.reload();
    }
  });
}
