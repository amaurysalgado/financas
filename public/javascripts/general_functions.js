String.prototype.capitalize = function() {
  return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
};

$(document).ready(function(){
  $(".dropdown").mouseover(function(){
    $(".dropdown-menu").show();
  });
  $(".dropdown").mouseout(function(){
    $(".dropdown-menu").hide();
  });
});
