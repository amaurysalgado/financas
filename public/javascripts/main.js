$(document).ready(function(){
  var graph = $("#graphs");
  if (graph.length > 0){
    var graph = new Cust();
    graph.drawBarChart();
    graph.drawPieChart();
    $("select#bill-kind").on('change', function(){
      graph.drawBarChart();
    })
  }

  // $('table').dataTable({responsive: true});
  $('button[data-method="delete"]').on("click", function(){
    let confirmDelete = true;
    if(this.dataset.confirm != null || this.dataset.confirm != ""){
      confirmDelete = confirm(this.dataset.confirm);
    }
  });
});
