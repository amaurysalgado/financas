class Cust {
  constructor() {
    this.barChart = echarts.init(document.getElementById('bar'));
    this.pieChart = echarts.init(document.getElementById('pie'));
  }

  _getData(url, callback){
    $.ajax({
      url: url,
      method: "GET",
      success: function(data) { callback(data); }
    })
  }

  drawBarChart(labels, values){
    let billKind = $("select#bill-kind")[0];
    let self = this;
    this._getData("/metrics/bill_history/"+billKind.value, function(monthsData){
      let option = self._getBarOptions(monthsData, billKind.selectedOptions[0].innerText)
      self.barChart.setOption(option);
    });
  }

  drawPieChart(){
    let self = this;
    this._getData("/metrics/last_month_bills", function(billsData){
      let data = [];
      let  i = 0;
      for (i; i < billsData.bills.length; i++){
        data.push({value: billsData.bills[i][1], name: billsData.bills[i][0]});
      }
      let options = self._getPieOptions(billsData,data)
      self.pieChart.setOption(options);
    });
  }

  _getBarOptions(monthsData, billName){
    let options = {
      title: { text: 'Custos totais ao longo dos meses' },
      tooltip : { trigger: 'item', formatter: "{a} <br/>{b} : {c}" },
      xAxis: { data: monthsData.date },
      yAxis: {},
      series: [{ name: billName, type: 'bar', data: monthsData.value }]
    };
    return options;
  }

  _getPieOptions(billsData, data){
    let options = {
      title: { text: `Custos do último mês (${billsData.month_year}) - (Total - ${billsData.total})` },
      tooltip : { trigger: 'item', formatter: "{b} : {c} ({d}%)" },
      series: [{ type: 'pie', data: data }]
    };
    return options;
  }
}
