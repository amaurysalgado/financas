"use_strict";
class Utils {
  constructor(options) {}

  permitedParams(object, permited_params){
    let response = {};
    let unpermitted = [];
    if(object != null){
      let object_keys = Object.keys(object);
      let i = 0;
      for(i; i < object_keys.length; i++){
        if (permited_params.includes(object_keys[i])){
          response[object_keys[i]] = object[object_keys[i]];
        }else{
          unpermitted.push(object_keys[i]);
        }
      }
    }
    if (unpermitted.length > 0){
      console.log("Unpermitted params: " + unpermitted.join(", ") );
    }
    return response;
  }

  monthYear(){
    let date = new Date();
    return {
      "month": (date.getMonth() < 9 ?  `0${date.getMonth()+1}` : date.getMonth()+1),
      "year": 1900+date.getYear()
    }
  }
}


module.exports = new Utils();
