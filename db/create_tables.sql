CREATE SCHEMA `finances` ;
CREATE TABLE `finances`.`users` (
  `id` INT NOT NULL, `name` VARCHAR(45) NULL,
  `salary` FLOAT NULL, PRIMARY KEY (`id`));

CREATE TABLE `finances`.`bills` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `value` FLOAT NOT NULL DEFAULT 0.0,
  `month` VARCHAR(45) NOT NULL DEFAULT '1',
  `year` INT NOT NULL DEFAULT 2017,
  `house` BIT NOT NULL DEFAULT 1,
  `amaury` BIT NOT NULL DEFAULT 0,
  `luana` BIT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`));


ALTER TABLE `finances`.`users`
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `finances`.`users`
ADD COLUMN `username` VARCHAR(45) NOT NULL DEFAULT AFTER `name`;
ALTER TABLE `finances`.`users`
ADD COLUMN `password` VARCHAR(45) NOT NULL DEFAULT AFTER `username` ;

ALTER TABLE `finances`.`bills`
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `finances`.`bills`
DROP COLUMN `luana`,
DROP COLUMN `amaury`,
ADD COLUMN `user_id` INT NULL AFTER `house`,
ADD INDEX `user_id_idx_idx` (`user_id` ASC);

ALTER TABLE `finances`.`bills`
ADD CONSTRAINT `user_id_idx`
  FOREIGN KEY (`user_id`)
  REFERENCES `finances`.`users` (`id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;

ALTER TABLE `finances`.`bills`
  DROP COLUMN `house`;

CREATE TABLE `finances`.`bills_types` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `finances`.`bills`
ADD COLUMN `bill_type_id` INT NULL AFTER `user_id`,
ADD INDEX `bill_type_id_idx_idx` (`bill_type_id` ASC);

ALTER TABLE `finances`.`bills`
ADD CONSTRAINT `bill_type_id_idx`
  FOREIGN KEY (`bill_type_id`)
  REFERENCES `finances`.`bills_types` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

INSERT INTO `finances`.`bills_types` (`id`,`name`) VALUES
  (1,'Agua'),(2,'Aluguel'),(3,'Energia'),(4,'Internet'),(5,'Segurança'),
  (6,'Netflix'),(7,'Cartão de Crédito');

Update `finances`.`bills` set `bill_type_id`='1' WHERE  `name` ='agua';
Update `finances`.`bills` set `bill_type_id`='2' WHERE  `name` ='aluguel';
Update `finances`.`bills` set `bill_type_id`='3' WHERE  `name` ='energia';
Update `finances`.`bills` set `bill_type_id`='4' WHERE  `name` ='internet';
Update `finances`.`bills` set `bill_type_id`='6' WHERE  `name` ='netflix';
