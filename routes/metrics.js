const express = require('express');
const router = express.Router();

router.route('/')
  .get(async function(req, res){
    types = await BillType.getAll();
    res.render('metrics/index',{billsTypes: types});
  });

router.route('/bill_history/:billName')
  .get(async function(req, res){
    let billName = req.params.billName;
    data = await Bill.getLastMonths(billName);
    res.json(data);
  });

router.route('/last_month_bills/')
  .get(async function(req, res){
    let billName = req.params.billName;
    data = await Bill.getLastMonthBills();
    res.json({bills: data.bills, month_year: data.month_year, total: data.total.toFixed(2)});
  });

module.exports = router;
