let express = require('express');
let router = express.Router();

router.route('/')
  .get(function(req, res){
    res.render('bills/index');
  })
  .post(async function(req, res){
    let newBill = billParams(req.body);
    newBill['house'] = 0;
    newBill[req.body["bill"]['owner']] = 1;
    newBill = await Bill.insert(newBill);
    res.redirect("/bills");
  });

router.route('/get_bills')
  .get(async function (req,res) {
    bills = await Bill.getAll();
    res.json({data: bills});
  })

router.route('/new')
  .get(async function(req,res){
    users = await User.getAll();
    billsType = await BillType.getAll();
    res.render('bills/new',{bill: Bill.getInitalObject(), users: users, billsTypes: billsType});
  });

router.route("/:id")
  .get(async function(req,res){
    bill = await Bill.getById(req.params.id);
    if (bill == null){
      res.redirect('');
    }else{
      user = await User.getById(bill.user_id);
      res.render('bills/show',{bill: bill, user: user});
    }
  })
  .post(async function(req,res){
    bill = Bill.getById(req.params.id);
    let newValues = billParams(req.body);
    Object.assign(bill, newValues);
    Bill.update(req.params.id,bill);
    res.redirect(`${req.params.id}`);
  })
  .delete(async function(req,res){
    await Bill.deleteById(req.params.id);
    res.json({response: 'ok'});
  });

router.route("/:id/edit")
  .get(async function(req,res){
    bill = await Bill.getById(req.params.id);
    if (bill == null){
      res.redirect('/bills');
    }else{
      users = await User.getAll();
      billsType = await BillType.getAll();
      res.render('bills/new',{bill: bill, users: users, billsTypes: billsType});
    }
  });

function billParams(object){
  return  Utils.permitedParams(object["bill"],["bill_type_id","value","month","year","user_id"]);
}

module.exports = router;
