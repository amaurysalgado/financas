let express = require('express');
let router = express.Router();

router.route('/')
  .get(async function(req, res){
    bills_types = await BillType.getAll();
    res.render('bills_types/index', {bills_types: bills_types});
  });

router.route('/:id/edit')
  .get(async function(req, res){
    let bill_type = await BillType.getById(req.params.id);
    res.render('bills_types/edit',{bill_type: bill_type});
  });

router.route('/:id')
  .post(async function(req, res){
    let bill_type = await BillType.update(req.params.id, billsTypeParams(req.body));
    res.redirect('/bills_types')
  });

function billsTypeParams(object){
  return  Utils.permitedParams(object["bill_type"],["name"]);
}

module.exports = router;
