var express = require('express');
var router = express.Router();

router.get('/',async function(req, res){
  let users = await User.getAll();
  res.render('users/index', {users: users });
});

router.route('/:id')
  .get(async function(req, res){
    let user = await User.getById(req.params.id);
    if( user ){
      res.render('users/show', {user: user });
    }else {
      res.redirect('/users');
    }
  })
  .post(async function(req, res){
    let user = await User.getById(req.params.id);
    let permited_params = userParams(req.body);
    if (await User.update(req.params.id, permited_params)){
      res.redirect(req.params.id);
    }
  });

router.get('/:id/edit',async function(req, res){
  let user = await User.getById(req.params.id);
  if( user ){
    res.render('users/edit', {user: user });
  }else{
    res.redirect('/users');
  }
});

function userParams(object){
  return  Utils.permitedParams(object["user"],["name","salary"]);
}

module.exports = router;
